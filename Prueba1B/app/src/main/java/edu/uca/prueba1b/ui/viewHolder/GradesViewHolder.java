package edu.uca.prueba1b.ui.viewHolder;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import edu.uca.prueba1b.R;
import edu.uca.prueba1b.modelos.Ordenes;
import edu.uca.prueba1b.ui.interfaces.ItemClickListener;

public class GradesViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener {

    private Ordenes mItem;
    private Context mContext;
    private TextView tv_producto;
    private TextView tv_precio;
    private ItemClickListener<Ordenes> mClickListener;

    public GradesViewHolder(View itemView, ItemClickListener<Ordenes> clickListener) {
        super(itemView);
        mContext = itemView.getContext();
        itemView.setOnClickListener(this);
        mClickListener = clickListener;
        tv_producto = (TextView) itemView.findViewById(R.id.tv_producto);
        tv_precio = (TextView) itemView.findViewById(R.id.tv_precio);

    }

    public void setItem(Ordenes item){
        mItem = item;
        tv_producto.setText(item.getmProducto());
        tv_precio.setText(String.valueOf(item.getmPrecio()));


    }

    @Override
    public void onClick(View v) {
        if(mClickListener != null){
            mClickListener.OnClick(mItem);
        }
    }
}

package edu.uca.prueba1b.modelos;

/**
 * Created by Administrador on 01/11/2017.
 */

public class Ordenes {

    private String mProducto;
    private float mPrecio;

    public Ordenes(String producto, float precio){
        mProducto = producto;
        mPrecio = precio;
    }

    public String getmProducto(){
        return mProducto;

    }

    public void setmProducto(String mProducto) {

        this.mProducto = mProducto;
    }

    public float getmPrecio(){
        return mPrecio;
    }

    public void setPrecio(float mPrecio){
        this.mPrecio = mPrecio;
    }
}

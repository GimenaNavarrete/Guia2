package edu.uca.prueba1b.ui.interfaces;

/**
 * Created by Administrador on 01/11/2017.
 */

public interface ItemClickListener<T> {
    void OnClick(T item);
}

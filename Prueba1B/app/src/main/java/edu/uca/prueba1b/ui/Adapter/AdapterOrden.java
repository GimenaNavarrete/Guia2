package edu.uca.prueba1b.ui.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import edu.uca.prueba1b.R;
import edu.uca.prueba1b.modelos.Ordenes;
import edu.uca.prueba1b.ui.interfaces.ItemClickListener;
import edu.uca.prueba1b.ui.viewHolder.GradesViewHolder;

/**
 * Created by Administrador on 01/11/2017.
 */

public class AdapterOrden extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Ordenes> mListItems;
    private ItemClickListener<Ordenes> mClickListener;


    public AdapterOrden(List<Ordenes> items, ItemClickListener<Ordenes> clickListener){
        mListItems = items;
        mClickListener = clickListener;
    }

    public void actualizar(List<Ordenes> newData){
        mListItems.clear();
        mListItems.addAll(newData);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_grades_view_holder,parent,false);
        return new GradesViewHolder(v, mClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        GradesViewHolder viewHolder = (GradesViewHolder) holder;
        viewHolder.setItem(mListItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }
}

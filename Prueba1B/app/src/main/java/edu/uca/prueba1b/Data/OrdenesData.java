package edu.uca.prueba1b.Data;

import java.util.ArrayList;
import java.util.List;

import edu.uca.prueba1b.modelos.Ordenes;

/**
 * Created by Administrador on 01/11/2017.
 */

public class OrdenesData {

    private static List<Ordenes> datos=null;

    private static List<Ordenes> cargar(){
        List<Ordenes> result = new ArrayList<>();
        result.add(new Ordenes("Arroz y pollo", 40.6f));
        result.add(new Ordenes("Pan y queso", 50.5f));
        result.add(new Ordenes("Queso y zalami", 60.5f));

        return result;
    }

    public static List<Ordenes> getData(){
        if(datos==null){
            datos = cargar();
        }
        return datos;
    }
}

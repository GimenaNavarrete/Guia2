package edu.uca.prueba1b.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.uca.prueba1b.Data.OrdenesData;
import edu.uca.prueba1b.R;
import edu.uca.prueba1b.modelos.Ordenes;
import edu.uca.prueba1b.ui.Adapter.AdapterOrden;
import edu.uca.prueba1b.ui.interfaces.ItemClickListener;

public class MainActivity extends AppCompatActivity {

    private List<Ordenes> mItemList;
    private RecyclerView rvData;
    private AdapterOrden AdapterOrden;
    private Activity mActivity;
    private Button btAgregar;
    private EditText etProducto;
    private EditText etPrecio;
    private TextView tv_Total;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mActivity = this;
        mItemList = new ArrayList<>();
        rvData = (RecyclerView) findViewById(R.id.rv_data);
        AdapterOrden = new AdapterOrden(mItemList, new ItemClickListener<Ordenes>() {
            @Override
            public void OnClick(Ordenes item) {

                Intent intent =
                        new Intent(mActivity, DetailOrden.class);

                intent.putExtra("Producto", item.getmProducto());

                intent.putExtra("Precio", String.valueOf(item.getmPrecio()));

                startActivity(intent);

                //Toast.makeText(mActivity,item.getmName(),Toast.LENGTH_SHORT).show();
            }
        });
        rvData.setAdapter(AdapterOrden);
        rvData.setHasFixedSize(true);
        LinearLayoutManager mLLCatalogData = new LinearLayoutManager(this);
        mLLCatalogData.setOrientation(LinearLayoutManager.VERTICAL);
        rvData.setLayoutManager(mLLCatalogData);
        mItemList.clear();
        mItemList.addAll(OrdenesData.getData());
        AdapterOrden.notifyDataSetChanged();

        tv_Total = (TextView) findViewById(R.id.tv_Total);
        tv_Total.setText(String.valueOf(calculos()));

        btAgregar = (Button) findViewById(R.id.btAgregar);
        etProducto = (EditText) findViewById(R.id.etProducto);
        etPrecio = (EditText) findViewById(R.id.etPrecio);
        btAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if(!(etPrecio.getText().toString().equals("") || etProducto.getText().toString().equals(""))){
                    Ordenes ord = new Ordenes(etProducto.getText().toString(), Float.parseFloat(etPrecio.getText().toString()));
                    OrdenesData.getData().add(ord);
                    AdapterOrden.actualizar(OrdenesData.getData());
                    etProducto.setText("");
                    etPrecio.setText("");
                    tv_Total.setText(String.valueOf(calculos()));
                }

            }
        });
    }

    public float calculos(){
        List<Ordenes> listTemp;
        listTemp = OrdenesData.getData();
        float temp=0.0f;
        for(int i=0; i<listTemp.size(); i++){
            temp=temp+listTemp.get(i).getmPrecio();
        }
        return temp;
    }
}

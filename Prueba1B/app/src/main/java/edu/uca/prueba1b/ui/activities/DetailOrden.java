package edu.uca.prueba1b.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.uca.prueba1b.R;

public class DetailOrden extends AppCompatActivity implements View.OnClickListener {

    TextView tv_producto;
    TextView tv_precio;
    private String mproducto;
    private float mprecio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_orden);

        tv_producto = (TextView) findViewById(R.id.tv_producto);
        tv_precio = (TextView) findViewById(R.id.tv_precio);



        Bundle example = getIntent().getExtras();
        if(example != null){
            mproducto = example.getString("mProducto");
            mprecio = Float.parseFloat(example.getString("mprecio"));
        }
        tv_producto.setText(mproducto);
        tv_precio.setText((int) mprecio);
    }

    @Override
    public void onClick(View view) {

    }
}
